import logo from './logo.svg';
import './App.css';
import { Parrafo } from './Parrafo.jsx';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Esta es una prueba para saber si le sirve a Marlys React.
        </p>
        < Parrafo />
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
